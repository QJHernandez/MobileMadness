﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class EnemyDataAssets
{
    [MenuItem("Own/ScriptableObjects/EnemyData")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<EnemyData>();
    }
}