﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class InputsAssets
{
    [MenuItem("Own/ScriptableObjects/Inputs")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<Inputs>();
    }
}