﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants {

	[System.Serializable]
    public class AnimatorParameters
    {
        public static string playerSpeed = "Speed";
    }

    [System.Serializable]
    public class PlayerAnimations
    {
        public static string run = "Run";
        public static string idle = "Idle";
        public static string jump = "Jump";
        public static string damaged = "Damaged";
    }

    public class GameScenes
    {
        public static string easy = "2_Easy";
        public static string hard = "1_Hard";
        public static string mainMenu = "0_MainMenu";
        public static string info = "Info";
    }
}
