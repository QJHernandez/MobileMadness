﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePlatform : MonoBehaviour {

    private PlayerController player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerController>();
        if (player)
        {
            player.transform.parent = gameObject.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        player = collision.GetComponent<PlayerController>();
        if (player)
        {
            player.transform.parent = null;
        }
    }
}
