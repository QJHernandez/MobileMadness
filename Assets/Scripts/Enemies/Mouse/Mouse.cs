﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour
{
    public EnemyData data;
    
    private float timer;
    private float speed;
    private bool keepChecking = true;
    private bool seen = false;
    private bool attacking = true;
    private bool waiting = false;
    private Vector2 direction;
    private SpriteRenderer sRenderer;
    private PlayerController player;
    private Enemy enemy;

    private void Awake()
    {
        enemy = GetComponent<Enemy>();
        enemy.RemainingHealth = data.totalHealth;
        sRenderer = GetComponent<SpriteRenderer>();
        speed = data.mouseData.attackSpeed;
        direction = Vector2.left;
    }

    private void Start()
    {
        StartCoroutine(CheckAtIntervals(data.mouseData.checkIntervals));
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (!waiting)
        {
            if (attacking)
            {
                if (timer >= data.mouseData.attackTime)
                {
                    direction = Vector2.right;
                    speed = data.mouseData.returnSpeed;
                    timer = 0;
                    attacking = false;
                }
            }
            else
            {
                if (timer >= data.mouseData.returnTime)
                {
                    direction = Vector2.left;
                    speed = data.mouseData.attackSpeed;
                    timer = 0;
                    waiting = true;
                }
            }
            transform.Translate(direction * speed * Time.deltaTime);
        }
        else
        {
            if (timer >= data.mouseData.waitTime)
            {
                waiting = false;
                attacking = true;
                timer = 0;
            }
        }          
    }

    void Check()
    {
        if (sRenderer.isVisible && seen == false)
        {
            seen = true;
        }
    }

    IEnumerator CheckAtIntervals(float secondsBetweenCheck)
    {
        // Repeat until keepSpawning == false or this GameObject is disabled/destroyed.
        while (keepChecking)
        {
            // Put this coroutine to sleep until the next spawn time.
            yield return new WaitForSeconds(secondsBetweenCheck);
            // Now it's time to spawn again.
            Check();
        }
    }

    private void OnTriggerEnter2D( Collider2D collision )
    {
        player = collision.GetComponent<PlayerController>();
        if (player)
        {
            player.ReceiveDamage(data.damage, false);
        }
    }
}

