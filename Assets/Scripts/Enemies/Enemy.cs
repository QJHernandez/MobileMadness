﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float remainingHealth;

    public float RemainingHealth
    {
        get { return remainingHealth; }
        set { remainingHealth = value; }
    }

    public void ReceiveDamage( float dmg )
    {
        remainingHealth -= dmg;
        if (remainingHealth <= 0)
        {
            //Todo when enemy dies
            remainingHealth = 0;
            Destroy(gameObject);
        }
    }
}
