﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Remote : MonoBehaviour {

    public EnemyData data;
    public GameObject shootPrefab;
    
    private bool seen = false;
    private bool keepSpawning = true;
    private GameObject shootPoint;
    private SpriteRenderer sRenderer;
    private Enemy enemy;

    private void Awake()
    {
        enemy = GetComponent<Enemy>();
        enemy.RemainingHealth = data.totalHealth;
        foreach (Transform child in transform)
        {
            shootPoint = child.gameObject;
        }
        sRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        StartCoroutine(SpawnAtIntervals(1/data.remoteData.fireRate));
    }

    public void InstantiateShot()
    {
        if (sRenderer.isVisible && seen == false)
        {
            seen = true;
        }
        else if(!sRenderer.isVisible && seen == true)
        {
            seen = false;
        }

        if (seen)
        {
            RemotesController.levelManager.PlayRemoteSFX(LevelManager.remotesController.shootSound);
            RemoteShot shot = Instantiate(shootPrefab, shootPoint.transform.position, shootPoint.transform.rotation).GetComponent<RemoteShot>();
            shot.speed = data.remoteData.shotSpeed;
            shot.damage = data.damage;
        }
    }

    IEnumerator SpawnAtIntervals(float secondsBetweenSpawns)
    {
        // Repeat until keepSpawning == false or this GameObject is disabled/destroyed.
        while (keepSpawning)
        {
                // Put this coroutine to sleep until the next spawn time.
                yield return new WaitForSeconds(secondsBetweenSpawns);
                // Now it's time to spawn again.
                InstantiateShot();
        }
    }
}
