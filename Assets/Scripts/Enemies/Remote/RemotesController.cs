﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemotesController : MonoBehaviour
{
    public AudioClip shootSound;

    public static LevelManager levelManager;

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }
}
