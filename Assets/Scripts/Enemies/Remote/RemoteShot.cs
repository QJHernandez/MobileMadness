﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteShot : MonoBehaviour {

    [HideInInspector]
    public float speed;
    public float damage;
    public float timeToDisable;

    private float timer;

	void Update ()
    {
        transform.Translate(Vector2.right * Time.deltaTime * speed);
        if (timer < timeToDisable)
        {
            timer += Time.deltaTime;
            if (timer > timeToDisable)
            {
                Destroy(gameObject);
            }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerController>())
        {
            collision.GetComponent<PlayerController>().ReceiveDamage(damage, false);
            Destroy(gameObject);
        }
    }
}
