﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [Header("ScriptableObjects")]
    public Inputs inputs;

    [Header("Audio Sources")]
    public AudioSource musicPlayer;
    public AudioSource sfxPlayer;
    public List<AudioSource> sfxEnemies;
    public AudioSource sfxCollectible;

    [Header("Audio Clips")]
    public AudioClip bgMUsicA;
    public AudioClip bgMUsicB;
        
    public static PlayerController playerController;
    public static GumsController gumsController;
    public static RemotesController remotesController;
    public static MouseController mouseController;
    public static AudioSource _sfxCollectibles;

    private bool isPaused = false;
    private UIManager _uiManager;

    private void Awake()
    {
        playerController = FindObjectOfType<PlayerController>();
        gumsController = FindObjectOfType<GumsController>();
        remotesController = FindObjectOfType<RemotesController>();
        mouseController = FindObjectOfType<MouseController>();
        _uiManager = FindObjectOfType<UIManager>();
        _sfxCollectibles = sfxCollectible;
    }

    private void Start()
    {
        StartCoroutine(PlayBGMusic());
    }

    private void Update()
    {
        if(Input.GetKeyDown(inputs.pause) || Input.GetKeyDown(inputs._pause))
        {
            Pause();
        }
    }

    private void Pause()
    {
        if (musicPlayer.isPlaying)
        {
            musicPlayer.Pause();
        }
        else
        {
            musicPlayer.UnPause();
        }
        isPaused = !isPaused;
        _uiManager.pauseScreen.SetActive(isPaused);
        _uiManager.HUD.SetActive(!isPaused);
        if(Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
        }
    }

    private void AssignSFXEnemy(AudioClip clip)
    {
        foreach(AudioSource audioSource in sfxEnemies)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.clip = clip;
                audioSource.Play();
                return;
            }
        }
    }

    public void PlayGumSFX(AudioClip clip)
    {
        AssignSFXEnemy(clip);
    }

    public void PlayRemoteSFX(AudioClip clip)
    {
        AssignSFXEnemy(clip);
    }

    public void PlayPlayerSound(AudioClip clip)
    {
        sfxPlayer.clip = clip;
        sfxPlayer.Play();
    }

    public void PauseMusic()
    {
        musicPlayer.Pause();
        if (sfxPlayer.isPlaying)
        {
            sfxPlayer.Pause();
        }
        foreach (AudioSource audioSource in sfxEnemies)
        {
            if (audioSource.isPlaying)
            {
                audioSource.Pause();
            }
        }
    }

    public void ResumeMusic()
    {
        musicPlayer.Play();
    }

    public void ContinuarButton()
    {
        Pause();
    }

    public void InfoButton()
    {

    }

    public void ReiniciarButton()
    {
        Time.timeScale = 1;
        if(SceneManager.GetActiveScene().name == Constants.GameScenes.hard)
        {
            SceneManager.LoadScene(Constants.GameScenes.hard);
        }
        else
        {
            SceneManager.LoadScene(Constants.GameScenes.easy);
        }
    }

    public void CerrarButton()
    {
        SceneManager.LoadScene(Constants.GameScenes.mainMenu);
    }

    IEnumerator PlayBGMusic()
    {
        musicPlayer.clip = bgMUsicA;
        musicPlayer.Play();

        yield return new WaitForSeconds(bgMUsicA.length);

        musicPlayer.clip = bgMUsicB;
        musicPlayer.Play();
        musicPlayer.loop = true;
    }
}