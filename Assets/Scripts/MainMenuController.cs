﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public AudioSource buttons;
    public AudioSource music;

    public void HardButton()
    {
        music.Pause();
        buttons.Play();
        Time.timeScale = 1;
        SceneManager.LoadScene(Constants.GameScenes.hard);
    }

    public void EasyButton()
    {
        music.Pause();
        buttons.Play();
        Time.timeScale = 1;
        SceneManager.LoadScene(Constants.GameScenes.easy);
    }

    public void InfoButton()
    {
        buttons.Play();
    }

    public void SalirButton()
    {
        music.Pause();
        buttons.Play();
        Application.Quit();
    }

}
