﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GumsController : MonoBehaviour
{
    public AudioClip stuckSound;

    public static LevelManager levelManager;

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
    }
}
