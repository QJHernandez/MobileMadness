﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gum : MonoBehaviour {

    public EnemyData data;
    
    private PlayerController player;
    private Enemy enemy;

    private void Awake()
    {
        enemy = GetComponent<Enemy>();
        enemy.RemainingHealth = data.totalHealth;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.gameObject.GetComponent<PlayerController>();
        if (player)
        {
            GumsController.levelManager.PlayGumSFX(LevelManager.gumsController.stuckSound);
            if(player.RemainingBattery > player.data.damages.vibrateDamage)
            {
                player.GetStuck();
            }
            else
            {
                player.PlayerDeath();
            }
        }
    }

}
