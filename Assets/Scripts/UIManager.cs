﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    [Header("Canvas Elements")]
    public GameObject HUD;
    public GameObject victoryScreen;
    public GameObject pauseScreen;
    [Header("HUD")]
    public Image battery;
    public Text batteryText;
    public Image wifi;
    public Text wifiText;
    [Header("Battery Sprites")]
    public Sprite battery1;
    public Sprite battery2;
    public Sprite battery3;
    [Header("Wifi Sprites")]
    public Sprite wifi0;
    public Sprite wifi1;
    public Sprite wifi2;
    public Sprite wifi3;

    private PlayerController player;

    private void Awake()
    {
        player = FindObjectOfType<PlayerController>();
    }

    public void ActivateVictory()
    {
        victoryScreen.SetActive(true);
        player.GetStuck();
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(0);
    }


}
