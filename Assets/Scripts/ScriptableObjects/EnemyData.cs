﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyData : ScriptableObject
{
    public float totalHealth;
    public float damage;
    public RemoteData remoteData;
    public MouseData mouseData;

    [System.Serializable]
    public class RemoteData
    {
        public float speed;
        public float fireRate;
        public float shotSpeed;
    }

    [System.Serializable]
    public class MouseData
    {
        public float checkIntervals;
        public float attackSpeed;
        public float attackTime;
        public float returnSpeed;
        public float returnTime;
        public float waitTime;
    }
}
