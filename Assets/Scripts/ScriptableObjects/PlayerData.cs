﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : ScriptableObject
{
    public float totalData;
    public float totalBattery;
    public Movement movement;
    public Damages damages;
    public Audio audio;
    public Sprites sprites;

    [System.Serializable]
    public class Damages
    {
        public float vibrateDamage;
        public float vibrateSelfDamage;
        public float flashDamage;
        public float flashSelfDamage;
    }

    [System.Serializable]
    public class Movement
    {
        public float gravity = -25f;
        public float runSpeed = 8f;
        public float vibrateSpeed = 2f;
        public float groundDamping = 20f;
        public float inAirDamping = 5f;
        public float jumpHeight = 3f;
        public float damageHeight = 0.3f;
        public float damagedMoveTime = 0.1f;
        public float damagedUnableToMoveTime = 0.5f;
    }

    [System.Serializable]
    public class Audio
    {
        public List<AudioClip> jump;
        public AudioClip flash;
        public AudioClip hurt;
        public AudioClip vibrate;

    }

    [System.Serializable]
    public class Sprites
    {
        public Sprite angry;
        public Sprite attack;
        public Sprite eyesClosed;
        public Sprite eyesOpen;
        public Sprite hurt;
        public Sprite intro;
        public Sprite jump;
        public Sprite vibration;
    }
}
