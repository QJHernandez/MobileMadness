﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : ScriptableObject
{
    [Header("Controls")]
    public KeyCode right = KeyCode.RightArrow;
    public KeyCode left = KeyCode.LeftArrow;
    public KeyCode jump = KeyCode.Space;
    public KeyCode flash = KeyCode.X;
    public KeyCode vibrate = KeyCode.Z;
    public KeyCode pause = KeyCode.Escape;

    [Header("Secondary Controls")]
    public KeyCode _right = KeyCode.D;
    public KeyCode _left = KeyCode.A;
    public KeyCode _jump = KeyCode.K;
    public KeyCode _flash = KeyCode.L;
    public KeyCode _vibrate = KeyCode.J;
    public KeyCode _pause = KeyCode.P;
}
