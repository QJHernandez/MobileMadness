﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vibration : MonoBehaviour {

    private PlayerController player;

    private void OnEnable()
    {
        player = transform.parent.transform.parent.gameObject.GetComponent<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        if (enemy)
        {
            enemy.ReceiveDamage(player.data.damages.vibrateDamage);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<Gum>())
        {
            player.Unstuck();

        }
    }

}
