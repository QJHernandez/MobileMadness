﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CharController2D;

public class PlayerController : MonoBehaviour
{
    [Header("ScriptableObjects")]
    public Inputs inputs;
    public PlayerData data;

    [Header("Objects to Assign")]
    public GameObject flashObject;
    public GameObject vibrationObject;

    public float RemainingBattery
    {
        get
        {
            return remainingBattery;
        }
    }
    
    private int normalizedHorizontalspeed = 0;
    private float remainingBattery;
    private bool stuck = false;
    private bool damaged = false;
    private bool damagedMove = false;
    private bool unableToMove = false;
    public bool vibrating = false;
    private bool damaging = false;
    private bool jumping = false;
    private CharacterController2D _controller;
    private UIManager _uiManager;
    private LevelManager _levelManager;
    private SpriteRenderer _spriteRenderer;
    [SerializeField]
    private Animator _animator;
    private RaycastHit2D _lastControllerColliderHit;
    private Vector3 _velocity;

    private void Awake()
    {
        _controller = GetComponent<CharacterController2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        _uiManager = FindObjectOfType<UIManager>();
        _levelManager = FindObjectOfType<LevelManager>();

        _controller.onControllerCollidedEvent += OnControllerCollider;
        _controller.onTriggerEnterEvent += OnTriggerEnterEvent;
        _controller.onTriggerExitEvent += OnTriggerExitEvent;

        //Reset data counter
        data.totalData = 0;

        //Sets the health back to full at the start of the game
        remainingBattery = data.totalBattery;
        UpdateBatteryHUD();
        _uiManager.wifiText.text = data.totalData.ToString();
    }

    private void Update()
    {
        if (_controller.isGrounded)
        {
            _velocity.y = 0;
            jumping = false;
        }
        

        if (damaged)
        {
            _velocity.y = Mathf.Sqrt(2f * data.movement.damageHeight * -data.movement.gravity);
            damaged = false;
        }

        if (damagedMove)
        {
            switch (transform.eulerAngles.y.ToString())
            {
                case "0":
                    normalizedHorizontalspeed = -1;
                    break;
                case "180":
                    normalizedHorizontalspeed = 1;
                    break;
            }
        }

        if (!stuck && !unableToMove)
        {
            if (Input.GetKey(inputs.right) || Input.GetKey(inputs._right))
            {
                normalizedHorizontalspeed = 1;
                transform.rotation = Quaternion.AngleAxis(0, Vector3.up);

                if (_controller.isGrounded)
                {
                    //Play animations to run
                    _animator.Play(Constants.PlayerAnimations.run);
                    if(_spriteRenderer.sprite != data.sprites.angry)
                    {
                        if (!damaging)
                        {
                            _spriteRenderer.sprite = data.sprites.angry;
                        }
                    }
                }
            }
            else if (Input.GetKey(inputs.left) || Input.GetKey(inputs._left))
            {
                normalizedHorizontalspeed = -1;
                transform.rotation = Quaternion.AngleAxis(180, Vector3.up);

                if (_controller.isGrounded)
                {
                    //Play animations to run 
                    _animator.Play(Constants.PlayerAnimations.run);
                    if (_spriteRenderer.sprite != data.sprites.angry)
                    {
                        if (!damaging)
                        {
                            _spriteRenderer.sprite = data.sprites.angry;
                        }
                    }
                }
            }
            else
            {
                if (!damagedMove)
                {
                    normalizedHorizontalspeed = 0;
                }
            }

            if (_controller.isGrounded && (Input.GetKeyDown(inputs.jump) || Input.GetKeyDown(inputs._jump)))
            {
                //Jumping actions
                //Play jump animations
                _animator.Play(Constants.PlayerAnimations.jump);
                jumping = true;
                _spriteRenderer.sprite = data.sprites.jump;
                _levelManager.PlayPlayerSound(data.audio.jump[Random.Range(0, data.audio.jump.Count)]);
                _velocity.y = Mathf.Sqrt(2f * data.movement.jumpHeight * -data.movement.gravity);
            }
        }
        else
        {
            if (!damagedMove)
            {
                normalizedHorizontalspeed = 0;
            }
        }

        if ((Input.GetKeyDown(inputs.flash)) || Input.GetKeyDown(inputs._flash))
        {
            //Flash actions
            //Vibrate Actions
            if (remainingBattery - data.damages.flashSelfDamage > 0)
            {
                StartCoroutine(Flash());
            }
        }

        if ((Input.GetKeyDown(inputs.vibrate)) || Input.GetKeyDown(inputs._vibrate))
        {
            //Vibrate Actions
            if (remainingBattery - data.damages.vibrateSelfDamage > 0)
            {
                StartCoroutine(Vibrate(vibrationObject));
            }
        }

        if (normalizedHorizontalspeed == 0 && _controller.isGrounded)
        {
            if (!damaging && !jumping)
            {
                _animator.Play(Constants.PlayerAnimations.idle);
                _spriteRenderer.sprite = data.sprites.eyesOpen;
            }
        }

        var smoothMovementFactor = _controller.isGrounded ? data.movement.groundDamping : data.movement.inAirDamping;

        if (!vibrating)
        {
            _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalspeed * data.movement.runSpeed, Time.deltaTime * smoothMovementFactor);
        }
        else
        {
            _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalspeed * data.movement.vibrateSpeed, Time.deltaTime * smoothMovementFactor);
        }


        _velocity.y += data.movement.gravity * Time.deltaTime;

        _controller.move(_velocity * Time.deltaTime);

        _velocity = _controller.velocity;
    }

    private void OnControllerCollider(RaycastHit2D hit)
    {
        if (hit.normal.y == 1f)
        {
            return;
        }
    }

    private void OnTriggerEnterEvent(Collider2D col)
    {
        //Debug.Log("onTriggerEnterEvent: " + col.gameObject.name);
    }

    private void OnTriggerExitEvent(Collider2D col)
    {
        //Debug.Log("onTriggerExitEvent: " + col.gameObject.name);
    }

    private IEnumerator Vibrate(GameObject vibrateSize)
    {
        ReceiveDamage(data.damages.vibrateSelfDamage, true);
        Sprite prevSprite = _spriteRenderer.sprite;

        //_levelManager.PlayPlayerSound(vibrateSound);
        _spriteRenderer.sprite = data.sprites.vibration;
        damaging = true;
        vibrating = true;
        vibrateSize.SetActive(true);


        yield return new WaitForSeconds(0.5f);

        _spriteRenderer.sprite = prevSprite;
        vibrateSize.SetActive(false);
        damaging = false;
        vibrating = false;
    }

    private IEnumerator Flash()
    {
        ReceiveDamage(data.damages.flashSelfDamage, true);
        Sprite prevSprite = _spriteRenderer.sprite;

        _levelManager.PlayPlayerSound(data.audio.flash);
        _spriteRenderer.sprite = data.sprites.attack;
        flashObject.SetActive(true);
        damaging = true;
        yield return new WaitForSeconds(0.25f);

        flashObject.SetActive(false);

        yield return new WaitForSeconds(0.25f);

        _spriteRenderer.sprite = prevSprite;
        damaging = false;
    }

    private IEnumerator DamagedMove()
    {
        damagedMove = true;
        yield return new WaitForSeconds(data.movement.damagedMoveTime);
        damagedMove = false;
    }

    private IEnumerator DisableMove()
    {
        Sprite prevSprite = _spriteRenderer.sprite;
        unableToMove = true;
        _spriteRenderer.sprite = data.sprites.hurt;
        _animator.Play(Constants.PlayerAnimations.damaged);
        yield return new WaitForSeconds(data.movement.damagedUnableToMoveTime);
        _spriteRenderer.sprite = prevSprite;
        unableToMove = false;
    }

    public void GetStuck()
    {
        stuck = true;
    }

    public void Unstuck()
    {
        stuck = false;
    }

    public void ReceiveHealing(float heal)
    {
        remainingBattery += heal;
        if(remainingBattery > data.totalBattery)
        {
            remainingBattery = data.totalBattery;
        }
        UpdateBatteryHUD();
    }

    public void ReceiveDamage(float dmg, bool own)
    {
        if (!unableToMove)
        {
            //Player actions after being damaged
            if (!stuck && !own)
            {
                _levelManager.PlayPlayerSound(data.audio.hurt);
                damaged = true;
                StartCoroutine(DamagedMove());
                StartCoroutine(DisableMove());
            }

            remainingBattery -= dmg;

            if (remainingBattery <= 0)
            {
                remainingBattery = 0;
                PlayerDeath();
            }

            UpdateBatteryHUD();
        }
    }

    public void AddData(float mb)
    {
        data.totalData += mb;
        _uiManager.wifiText.text = data.totalData.ToString();
    }

    public void PlayerDeath()
    {
        //Everything that happens when the player dies
        if (SceneManager.GetActiveScene().name == Constants.GameScenes.hard)
        {
            SceneManager.LoadScene(Constants.GameScenes.hard);
        }
        else
        {
            SceneManager.LoadScene(Constants.GameScenes.easy);
        }
    }

    private void UpdateBatteryHUD()
    {
        float percentage;
        string per;

        percentage = (remainingBattery * 100) / data.totalBattery;

        if(percentage <= 33)
        {
            _uiManager.battery.sprite = _uiManager.battery1;
        }
        else if(percentage <= 66)
        {
            _uiManager.battery.sprite = _uiManager.battery2;
        }
        else if(percentage <= 100)
        {
            _uiManager.battery.sprite = _uiManager.battery3;
        }

        per = percentage + "%";
        _uiManager.batteryText.text = per;
    }

    public void PlaySound(AudioClip clip)
    {
        _levelManager.PlayPlayerSound(clip);
    }
}