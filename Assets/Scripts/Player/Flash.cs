﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour {

    private PlayerController player;

    private void OnEnable()
    {
        player = transform.parent.gameObject.GetComponent<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        if (enemy && !collision.GetComponent<Gum>())
        {
            enemy.ReceiveDamage(player.data.damages.flashDamage);
        }
    }
}
